import { Component, OnInit, Input } from '@angular/core';
import { ItemsService } from '../items.service';






@Component({
  selector: 'item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})

export class ItemComponent implements OnInit {
  @Input() data:any;
  name;
  price;
  key;
  stock;
  showTheButton=false;
  showTheButton1=false;
  checkboxFlag: boolean;

  constructor(private ItemsService:ItemsService) { }

  delete() {
    this.ItemsService.deleteitems(this.key);
  } 
 
ngOnInit() {
  this.name = this.data.name;
  this.price = this.data.price;
  this.key = this.data.$key;
  this.checkboxFlag = this.data.stock;


}


checkChange()  {
  this.ItemsService.updateDone(this.key,this.name,this.checkboxFlag);
 }


showButton(){
  if(!this.showTheButton1)
  this.showTheButton = true;

}
hideButton(){
  this.showTheButton = false;

}
showButton1(){
  this.showTheButton1 = true;
  this.showTheButton=false;


}
hideButton1(){
  this.showTheButton1 = false;

}





}
